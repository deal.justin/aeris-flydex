# Flydex

The Flydex suggests if current conditions are favorable for taking Fly (a very good boy) outside for an extended period of time in an attempt to tire him out.

Conditions for Fly to play hard:
* cool temperatures (~50&deg;F)
* low humidity (<60&deg;F)
* low wind (<10mph)
* morning or evening hours (in range of 7-11am or 5-9pm)

These factors are considered equally when evaluating the index. The index can be interpreted as follows:
* 1 - Indoor throw day, prepare yourself for impatience and woofing if you try to do something else
* 2 - You might get a few jogs out of Fly before he gets hot, so expect lots of indoor throw today
* 3 - Paired with a second session in the day, Fly could get some exercise in and calm down
* 4 - You'll need to wash Fly off after he cools off in the creek, but you got some hard running out of him!
* 5 - Now's your chance! Fly will show off Border Collie endurance and then nap hard in a pile of paws tonight
