package indices

import (
	"testing"
	"time"
)

func TestCalculateTemperatureFactor(t *testing.T) {
	var tests = map[float64]float64{
		30.0: 0.5,
		40.0: 0.75,
		50.0: 1.0,
		60.0: 0.75,
		70.0: 0.5,
		80.0: 0.25,
	}

	for temperature, expected := range tests {
		var received = calculateTemperatureFactor(temperature)
		if received != expected {
			t.Errorf("Using temperature %2.1f, expected %2.2f, received %2.2f", temperature, expected, received)
		}
	}
}

func TestCalculateDewPointFactor(t *testing.T) {
	var tests = map[float64]float64{
		55.0: 1.0,
		60.0: 1.0,
		65.0: 0.75,
		70.0: 0.5,
		75.0: 0.25,
		80.0: 0.0,
	}

	for dewPoint, expected := range tests {
		var received = calculateDewPointFactor(dewPoint)
		if received != expected {
			t.Errorf("Using dew point %2.1f, expected %2.1f, received %2.1f", dewPoint, expected, received)
		}
	}
}

func TestCalculateWindSpeedFactor(t *testing.T) {
	var tests = map[float64]float64{
		5.0:  1.0,
		10.0: 0.5,
		15.0: 0.0,
		20.0: 0.0,
	}

	for windSpeed, expected := range tests {
		var received = calculateWindSpeedFactor(windSpeed)
		if received != expected {
			t.Errorf("Using wind speed %2.1f, expected %2.1f, received %2.1f", windSpeed, expected, received)
		}
	}
}

func TestCalculateTimeOfDayFactor(t *testing.T) {
	var tests = map[int]float64{
		10: 1,
		12: 0.5,
		22: 0,
	}

	for hour, expected := range tests {
		var timeOfDay = time.Date(2019, 5, 27, hour, 0, 0, 0, time.UTC)
		var received = calculateTimeOfDayFactor(timeOfDay)

		if received != expected {
			t.Errorf("Using time %s, expected %2.1f, received %2.1f", timeOfDay, expected, received)
		}
	}
}

func TestCalculateFlydex(t *testing.T) {
	var tempF = 55.
	var dewPointF = 45.
	var windSpeedMPH = 5.
	var timeOfDay = time.Date(2019, 5, 27, 10, 0, 0, 0, time.UTC)

	var received = CalculateFlydex(tempF, dewPointF, windSpeedMPH, timeOfDay)
	var expectedFlydex = 5

	if received != expectedFlydex {
		t.Errorf("Using time %s, expected %d, received %d", timeOfDay, expectedFlydex, received)
	}
}
