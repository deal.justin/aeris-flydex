package indices

import (
	"math"
	"time"
)

func calculateTemperatureFactor(temperature float64) float64 {
	// temperature is ideally 50F, losing 0.25 from the index every 10F deviation
	var ideal = 50.
	var step = 10.
	var lossPerStep = 0.25

	var stepsDeviated = int(math.Abs(ideal-temperature) / step)
	var indexLossUnclamped = float64(stepsDeviated) * lossPerStep
	var indexLoss = math.Min(1, indexLossUnclamped)

	return 1 - indexLoss
}

func calculateDewPointFactor(dewPoint float64) float64 {
	if dewPoint <= 60 {
		return 1
	}

	// dew point is ideally 60F, losing 0.25 from the index every 5F deviation (warmer only)
	var ideal = 60.
	var step = 5.
	var lossPerStep = 0.25

	var stepsDeviated = int(math.Abs(ideal-dewPoint) / step)
	var indexLossUnclamped = float64(stepsDeviated) * lossPerStep
	var indexLoss = math.Min(1, indexLossUnclamped)

	return 1 - indexLoss
}

func calculateWindSpeedFactor(windSpeed float64) float64 {
	// wind speed falls into one of three bins:
	// ideal: < 10mph (1)
	// good: 10-15mph (0.5)
	// poor: > 15mph (0)

	var index = 1.0

	if windSpeed >= 10 {
		index -= 0.5
	}

	if windSpeed >= 15 {
		index -= 0.5
	}

	return index
}

func calculateTimeOfDayFactor(timeOfDay time.Time) float64 {
	// time of day is ideally morning or evening
	// ideal: 7-11am or 5-9pm (1)
	// good: 11am-5pm (0.5)
	// poor: 9pm-7am (0)

	var index = 1.0

	switch timeOfDay.Hour() {
	case 11, 12, 13, 14, 15, 16:
		index -= 0.5
	case 21, 22, 23, 0, 1, 2, 3, 4, 5, 6:
		index--
	}

	return index
}

// CalculateFlydex based on current conditions and returns 1-5 (least to most favorable).
func CalculateFlydex(temperature float64, dewPoint float64, windSpeed float64, timeOfDay time.Time) int {
	// the index is a simply accumulation of each factor. Each factor can contribute at most
	// +1 to the index
	var index = 1.0

	index += calculateTemperatureFactor(temperature)
	index += calculateDewPointFactor(dewPoint)
	index += calculateWindSpeedFactor(windSpeed)
	index += calculateTimeOfDayFactor(timeOfDay)

	return int(math.Round(index))
}
