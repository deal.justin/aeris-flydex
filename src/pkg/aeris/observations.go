package aeris

import (
	"encoding/json"
	"fmt"
	"time"
)

// Location defines a place in the world
type Location struct {
	Long float64
	Lat  float64
}

// RelativeTo describes how far the observation is from our requested location
type RelativeTo struct {
	DistanceMI float64
}

// Observation data for a specific location and time
type Observation struct {
	DateTimeISO  time.Time
	TempF        *float64
	DewPointF    *float64
	WindSpeedMPH *float64
}

// ObservationResponse from the /observation endpoint
type ObservationResponse struct {
	ID         string
	Loc        Location
	Ob         Observation
	RelativeTo RelativeTo
}

// Observation for the provided location using the Aeris API
func (api *API) Observation(location string) (ObservationResponse, error) {

	// limit the fields we need - would be cool to somehow reflect this from the structs above
	api.Fields = "id,relativeTo.distanceMI,ob.dateTimeISO,ob.tempF,ob.dewpointF,ob.windSpeedMPH"

	// make the call and unmarshal into our structs above
	var response, err = api.get("observations", location)
	if err != nil {
		return ObservationResponse{}, err
	}

	var obsResponses = make([]ObservationResponse, 0)
	json.Unmarshal(*response.Response, &obsResponses)

	if len(obsResponses) == 0 {
		return ObservationResponse{}, fmt.Errorf("No observation found for %s", location)
	}

	return obsResponses[0], nil
}
