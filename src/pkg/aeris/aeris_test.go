package aeris

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"strings"
	"testing"
)

// helper to return a known test API definition
func createTestAPI(URL string) *API {
	return &API{
		Domain:       URL,
		ClientID:     "testid",
		ClientSecret: "testkey",
		Format:       "json",
		Filter:       "allstations",
		Limit:        1,
		Fields:       "one,two",
	}
}

// helper to return a test server that will spit back the provided json string as bytes
func testServerJSONResponse(testdata string) *httptest.Server {
	jsonData, err := ioutil.ReadFile(testdata)
	if err != nil {
		panic(err)
	}

	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(jsonData))
	}))
}

func TestNewAPI(t *testing.T) {
	var api = NewAPI("testid", "testkey")

	if api.ClientID != "testid" || api.ClientSecret != "testkey" {
		t.Errorf("Client credentials did not get set correctly with NewAPI.")
	}
}

func TestBuildURL(t *testing.T) {
	var api = createTestAPI("localhost")
	var URL = api.buildURL("observations", "omaha,ne")

	var expecting = map[string]string{
		"domain":   api.Domain,
		"endpoint": "observations",
		"location": url.QueryEscape("omaha,ne"),
		"format":   api.Format,
		"filter":   api.Filter,
		"limit":    strconv.Itoa(api.Limit),
		"fields":   url.QueryEscape(api.Fields),
	}

	for key, val := range expecting {
		if !strings.Contains(URL, val) {
			t.Errorf("Missing '%s' for parameter %s in URL %s", val, key, URL)
		}
	}
}

func TestBadGet(t *testing.T) {
	ts := testServerJSONResponse("testdata/aeris_api_error.json")

	var api = createTestAPI("localhost")
	api.Domain = ts.URL

	var _, err = api.Observation("omaha,ne")
	if err == nil {
		t.Errorf("Was expecting an error from the API, but received none.")
	}
}

func TestGoodGet(t *testing.T) {
	ts := testServerJSONResponse("testdata/good_observation.json")

	var api = createTestAPI("localhost")
	api.Domain = ts.URL

	var _, err = api.Observation("omaha,ne")
	if err != nil {
		t.Errorf("Wasn't expecting an error with good JSON input: %s", err)
	}
}
