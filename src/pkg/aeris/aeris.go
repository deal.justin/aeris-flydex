package aeris

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

// APIError contains any error information that came back with the APIResponse
type APIError struct {
	Code        string
	Description string
}

// APIResponse provides the minimum structure that could come back from an request
type APIResponse struct {
	Success  bool
	Error    *APIError
	Response *json.RawMessage
}

// API provides various functions that call out to Aeris
type API struct {
	Domain       string
	ClientID     string
	ClientSecret string
	Format       string
	Filter       string
	Limit        int
	Fields       string
}

// NewAPI returns a production facing API instance
func NewAPI(clientID string, clientSecret string) *API {

	// set some sensible defaults
	return &API{
		Domain:       "https://api.aerisapi.com",
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Format:       "json",
		Filter:       "allstations",
		Limit:        1,
	}
}

// BuildURL will generate the API call to Aeris given the current state of the object
func (api *API) buildURL(endpoint string, location string) string {
	URL, _ := url.Parse(api.Domain)
	URL.Path += fmt.Sprintf("%s/closest", endpoint)

	params := url.Values{}
	params.Add("p", location)
	params.Add("format", api.Format)
	params.Add("filter", api.Filter)
	params.Add("limit", strconv.Itoa(api.Limit))
	params.Add("fields", api.Fields)
	params.Add("client_id", api.ClientID)
	params.Add("client_secret", api.ClientSecret)

	URL.RawQuery = params.Encode()

	return URL.String()
}

// Get will make the API call and return an APIResponse if successful
func (api *API) get(endpoint string, location string) (APIResponse, error) {
	URL := api.buildURL(endpoint, location)

	// make the request
	response, err := http.Get(URL)
	if err != nil {
		return APIResponse{}, fmt.Errorf("GET failed against API: %s", err)
	}

	// read the data
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return APIResponse{}, fmt.Errorf("Failed to read response data: %s", err)
	}

	// unmarshal into our response type
	apiResponse := APIResponse{}
	err = json.Unmarshal(body, &apiResponse)
	if err != nil {
		return APIResponse{}, fmt.Errorf("Failed to unmarshal API response: %s", err)
	}

	// check for an Aeris error
	if apiResponse.Error != nil {
		return APIResponse{}, fmt.Errorf("Aeris reported an error: %s (%s)", apiResponse.Error.Description, apiResponse.Error.Code)
	}

	return apiResponse, nil
}
