package aeris

import (
	"fmt"
	"testing"
)

func TestObservation(t *testing.T) {
	ts := testServerJSONResponse("testdata/good_observation.json")

	var api = createTestAPI(ts.URL)
	var _, err = api.Observation("omaha,ne")
	if err != nil {
		panic(fmt.Errorf("Good observation response wasn't successfully parsed: %s", err))
	}
}

// this test is for verifying that we're going to consider a "response" field with
// a single observation in it as unsupported. Happens with the /observation/:place that
// I was using originally before switching to /observation/closest?p=:place which always
// returns a list of observations, even if I limit it to 1.
func TestUnsupportedSingleObservationResponse(t *testing.T) {
	ts := testServerJSONResponse("testdata/bad_observation.json")

	var api = createTestAPI(ts.URL)
	var _, err = api.Observation("omaha,ne")
	if err == nil {
		t.Fatal("No error was thrown parsing an unsupported response, so thats not good")
	}
}
