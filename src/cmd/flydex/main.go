package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"github.com/logrusorgru/aurora"
	"gitlab.com/deal.justin/aeris-flydex/src/pkg/aeris"
	"gitlab.com/deal.justin/aeris-flydex/src/pkg/indices"
)

func colorizedFlydex(flydex int) aurora.Value {
	switch flydex {
	case 1:
		return aurora.BrightRed(flydex)
	case 2:
		return aurora.BrightYellow(flydex)
	case 3:
		// this 202 number took way too long to figure out
		return aurora.Index(202, flydex)
	case 4:
		return aurora.BrightCyan(flydex)
	case 5:
		return aurora.BrightGreen(flydex)
	default:
		return aurora.BrightWhite(flydex)
	}
}

func main() {
	// command line args
	var loc = flag.String("loc", "", "location to check (ex: omaha,ne or 68122 or 42.1,-100)")

	flag.Parse()
	if flag.NFlag() == 0 {
		flag.Usage()
		os.Exit(1)
	}

	// grab the Aeris client ID/secret key from the env vars
	var clientID = os.Getenv("AERIS_CLIENT_ID")
	var clientSecret = os.Getenv("AERIS_CLIENT_SECRET")
	if clientID == "" || clientSecret == "" {
		fmt.Printf("Aeris client ID or secret key missing from environment. Please set AERIS_CLIENT_ID and AERIS_CLIENT_SECRET")
		os.Exit(1)
	}

	// hit the AerisWeather API for current conditions
	var api = aeris.NewAPI(clientID, clientSecret)

	var obsResponse, err = api.Observation(*loc)
	if err != nil {
		fmt.Printf("Error with API call: %s\n", err)
		os.Exit(1)
	}

	// calculate the Flydex
	var ob = obsResponse.Ob
	var flydex = indices.CalculateFlydex(*ob.TempF, *ob.DewPointF, *ob.WindSpeedMPH, ob.DateTimeISO)

	// fancy output
	var message = `
Requested Location:		%s
Observation Location:	%s (%.1f miles away)
Temp (F):				%2.1f
Dew Point (F):			%2.1f
Wind Speed (MPH):		%2.1f
Time (Local):			%s

Calculated Flydex:		%d
`

	fmt.Printf(message,
		*loc,
		obsResponse.ID, obsResponse.RelativeTo.DistanceMI,
		*ob.TempF,
		*ob.DewPointF,
		*ob.WindSpeedMPH,
		ob.DateTimeISO.Format(time.RFC822),
		colorizedFlydex(flydex))
}
